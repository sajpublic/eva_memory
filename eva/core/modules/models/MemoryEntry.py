import mongoengine


class MemoryEntryContent(mongoengine.EmbeddedDocument):
    workflow_execution_memory_entry_identifier = mongoengine.StringField()
    workflow_execution_memory_entry_data = mongoengine.StringField()
    meta = {
        'db_alias': 'memory-alias'
    }


class MemoryEntry(mongoengine.Document):
    workflow_execution_id = mongoengine.StringField()
    workflow_execution_memory_entry = mongoengine.EmbeddedDocumentField(MemoryEntryContent)
    meta = {
        'db_alias': 'memory-alias'
    }

import logging

import mongoengine
from pymongo import MongoClient, errors
from log import logger
from mongoengine import connect
from eva.core.modules.models.MemoryEntry import MemoryEntry, MemoryEntryContent
from typing import Dict, Text, Any, List, Union, Optional


class MemoryEngine:
    """
    EVA Module MemoryEngine offers the possibility to interfere with the global memory of EVA.

    Attributes
    ----------
    logger : logger
        A global logger.
    client : MongoClient
        An instance of MongoClient. Care: Lazy loaded.
    db : Collection
        A MongoDB collection.

    """

    def __init__(self, host, port):
        logger.setup_logging()
        self.logger = logging.getLogger(__name__)
        self.client = MongoClient(f"mongodb://{host}:27017")
        self.host = host
        self.db = self.client['memory']
        connect(
            alias='memory-alias',
            db='memory',
            host=f"mongodb://{self.host}:27017",
            port=27017
        )

    def get_memory_entries(self, workflow_execution_id):
        # type: (Text) -> [mongoengine.Document, bool]
        """Returns all documents in EVA's memory.

        Parameters
        ----------
        :workflow_execution_id: str The uniquely generated workflow execution id EVA refers to

        Raises
        ------
        MemoryNotFound
            If memory collection wasn't found on host.
        """
        if self.db is not None:
            return MemoryEntry.objects(workflow_execution_id=workflow_execution_id)
        else:
            self.logger.error('____eva.modules.memory.memoryengine.MemoryNotFound')

    def add_memory_entry(self, workflow_execution_id, workflow_execution_memory_entry):
        # type: (Text, Dict[Text, Any]) -> [mongoengine.Document, bool]
        """Adds a memory entry with mongoengine.

        Parameters
        ----------
        :workflow_execution_id: str The uniquely generated workflow execution id EVA refers to
        :workflow_execution_memory_entry: dict The content of the current entry
            :workflow_execution_memory_entry_identifier: str identifier of the current entry
            :workflow_execution_memory_entry_data: str generic data

        Raises
        ------
        FailedToAddMemoryEntry
            If mongoengine wasn't able to connect to the MongoClient.
        """
        try:
            memory_entry_object = MemoryEntry(workflow_execution_id=workflow_execution_id,
                                 workflow_execution_memory_entry=workflow_execution_memory_entry).save()
            return memory_entry_object
        except mongoengine.errors.FieldDoesNotExist as eFDE:
            self.logger.error('____eva.modules.memory.memoryengine {}'.format(eFDE))
            return False

    def clean_memory_data(self, workflow_execution_id):
        """

        :param workflow_execution_id:
        :return:
        """
        try:
            MemoryEntry.objects(workflow_execution_id=workflow_execution_id).delete()
        except:
            self.logger.error('____eva.modules.memory.memoryengine.clean_memory_data error removing entries.')

    def get_memory_entry_with_identifier(self, workflow_execution_id, workflow_execution_memory_entry_identifier,
                                         as_object=False):
        # type: (Text, Text, Optional[bool]) -> [mongoengine.Document, bool]
        """Returns a memory entry based on workflow_execution_id and workflow_execution_memory_entry_identifier

        Parameters
        ----------
        :workflow_execution_id: str The uniquely generated workflow execution id EVA refers to
        :workflow_execution_memory_entry_identifier: str identifier of the current entry

        Raises
        ------
        FailedToAddMemoryEntry
            If mongoengine wasn't able to connect to the MongoClient.
        """

        try:
            if as_object:
                memory_entry = MemoryEntry.objects.filter(
                    workflow_execution_id=workflow_execution_id,
                    workflow_execution_memory_entry__workflow_execution_memory_entry_identifier=workflow_execution_memory_entry_identifier
                ).first()
                return memory_entry
            memory_entry = MemoryEntry.objects.filter(
                workflow_execution_id=workflow_execution_id,
                workflow_execution_memory_entry__workflow_execution_memory_entry_identifier=workflow_execution_memory_entry_identifier
            ).first().to_json()
            return memory_entry
        except:
            return None

    def update_memory_entry(self, workflow_execution_id, workflow_execution_memory_entry_identifier, entry_payload):
        # type: (Text, Text, Text) -> [mongoengine.Document, bool]
        """Updates a memory entry with the given payload

        Parameters
        ----------
        :workflow_execution_id: str The uniquely generated workflow execution id EVA refers to
        :workflow_execution_memory_entry_identifier: str identifier of the current entry
        :entry_payload: the payload for the update

        Raises
        ------
        FailedToAddMemoryEntry
            If mongoengine wasn't able to connect to the MongoClient.
        """
        try:
            memory_entry_to_update = self.get_memory_entry_with_identifier(workflow_execution_id=workflow_execution_id,
                                                                           workflow_execution_memory_entry_identifier=workflow_execution_memory_entry_identifier,
                                                                           as_object=True)
            if memory_entry_to_update is None:
                self.logger.error('____eva.modules.memory.memoryengine Entry is None')

            MemoryEntry.objects(id = memory_entry_to_update.id).update(
                set__workflow_execution_memory_entry__workflow_execution_memory_entry_data=entry_payload
            )
            return True

        except mongoengine.errors.FieldDoesNotExist as eFDE:
            self.logger.error('____eva.modules.memory.memoryengine {}'.format(eFDE))
            return False


if __name__ == '__main__':
    me = MemoryEngine(host='localhost', port=27017)
    me.clean_memory_data(workflow_execution_id='9d7275e9-a567-11ea-ad2e-0242ac120007')
    # print([entry.to_json() for entry in me.get_memory_entries(workflow_execution_id='123abc')])
    # entry = dict(
    #     workflow_execution_memory_entry_identifier='fdgdfg',
    #     workflow_execution_memory_entry_data='ghjghjghj'
    # )
    #
    # me.add_memory_entry(workflow_execution_id='123abc',  workflow_execution_memory_entry=entry)
    #
    # print(me.get_memory_entry_with_identifier(workflow_execution_id='123abc', workflow_execution_memory_entry_identifier='test'))
    # print(me.update_memory_entry(workflow_execution_id='123abc',
    #                                           workflow_execution_memory_entry_identifier='test', entry_payload='test_123'))


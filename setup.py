from setuptools import setup, find_packages

setup(name='eva.core.modules.memory',
      version='0.1.1',
      description='',
      url='https://gitlab.com/susiandjames/eva/core/modules/memory',
      author='Julian Gerhard',
      author_email='julian.gerhard@susiandjames.com',
      license='',
      packages=find_packages(),
      zip_safe=False,
      install_requires=['mongoengine', 'pymongo']
)